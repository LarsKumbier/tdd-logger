<?php

namespace KITS\Log;


class LoggerTest extends \PHPUnit_Framework_TestCase {
    private $logger;
    private $message = 'This is a Test Message';

    public function setUp()
    {
        $this->logger = new Logger();
    }

    public function test_ImplementsInterface()
    {
        $expected = 'Psr\Log\LoggerInterface';
        $actual = class_implements($this->logger);
        $this->assertContains($expected, $actual, 'Class should implement '.$expected.' - got '.implode(',', $actual));
    }

    /**
     * @dataProvider allLevelSpecificMethods
     */
    public function test_CallToErrorLevelFunctionHasSameResultAsCallingGenericLogFunction($method)
    {
        $mock = new LoggerTest\LoggerMock();
        $this->assertEmpty($mock->logWasCalled(), 'log() should have never been called');
        $mock->$method($this->message);
        $expected = array(
            array(
                'level' => $method,
                'message' => $this->message,
                'context' => array()
            ));
        $this->assertEquals($expected, $mock->logWasCalled(), 'log() should have been called once after calling '.$method.'()');
    }

    public function allLevelSpecificMethods()
    {
        return array(
            array('emergency'),
            array('alert'),
            array('critical'),
            array('error'),
            array('warning'),
            array('notice'),
            array('info'),
            array('debug')
        );
    }


    public function test_CallToLogThrowsExceptionIfErrorLevelUnknown()
    {
        $this->setExpectedException('Psr\Log\InvalidArgumentException');
        $this->logger->log('userlevel', $this->message);
    }


    public function test_LogAcceptsObjectsWithTypecastToString()
    {
        $string = "Massa";
        $obj = new LoggerTest\ObjectWithToStringMethod($string);
        $this->assertFalse($obj->toStringCalled());
        $this->logger->log('notice', $obj);
        $this->assertTrue($obj->toStringCalled());
    }

    /**
     * @dataProvider validMessagesWithContexts
     */
    public function test_ContextPlaceholdersAreReplacedAccordingly($message, $context, $expected)
    {
        $mock = new LoggerTest\LoggerMock();
        $actual = $mock->callReplaceContextInMessage($message, $context);
        $this->assertEquals($expected, $actual);
    }

    public function validMessagesWithContexts()
    {
        $context = array (
            'foo'  => 'bar',
            '0815' => '42',
            '--'   => 'märk'
        );

        return array(
            array('A{foo}B',          $context, 'AbarB'      ),
            array('A {0815} B',       $context, 'A 42 B'     ),
            array('A {foo}B{0815} C', $context, 'A barB42 C' ),
            array('A {foo}B{foo} C',  $context, 'A barBbar C'),
            array('A {--} C',         $context, 'A märk C'   )
        );
    }


    /**
     * @depends test_ContextPlaceholdersAreReplacedAccordingly
     * @dataProvider invalidContexts
     */
    public function test_ContextPlaceholdersWithWhitespaceInDelimitersIsIgnored($message)
    {
        $context = array ('key' => 'value');
        $logger = new LoggerTest\LoggerMock();
        $expected = $message;
        $actual = $logger->callReplaceContextInMessage($message, $context);
        $this->assertEquals($expected, $actual);
    }

    public function invalidContexts()
    {
        return array (
            array('Invalid { key} context'),
            array('Invalid {key } context'),
            array('Invalid { key } context')
        );
    }
}
