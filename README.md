Solution for PSR-3 Logger Kata
==============================
Author: Lars Kumbier <lars@kumbier.it>

A solution for the implementation of a PSR-3 compliant Logger.


Requirements
============
- php 5.3+
- composer (http://getcomposer.org)
- all other requirements are installed via composer


Installation
============
- clone repository (with --depth=1)
- run `composer update` to install all required files


Usage
=====
See http://php-fig.org/psr/psr-3 or docs/psr-3.md


Directory Structure
===================
- `docs/`     External Documentation
- `lib/`      Application Libraries
- `tests/`    PHPunit tests
- `tmp/`      A temporary folder, e.g. for code coverage reports
- `vendor/`   automatically filled via `composer update`