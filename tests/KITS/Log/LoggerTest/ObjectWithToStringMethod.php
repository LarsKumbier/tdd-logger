<?php


namespace KITS\Log\LoggerTest;


class ObjectWithToStringMethod 
{
    private $toString;
    private $toStringCalled = false;

    public function __construct($toString)
    {
        $this->toString = $toString;
    }

    public function __toString()
    {
        $this->toStringCalled = true;
        return $this->toString;
    }

    public function toStringCalled()
    {
        return $this->toStringCalled;
    }
} 