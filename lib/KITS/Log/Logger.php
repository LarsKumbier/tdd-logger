<?php

namespace KITS\Log;


class Logger extends \Psr\Log\AbstractLogger
{
    protected $errorLevels;

    public function __construct()
    {
        $refl = new \ReflectionClass('\Psr\Log\LogLevel');
        $this->errorLevels = $refl->getConstants();
    }


    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        if (!$this->isValidErrorLevel($level))
            throw new \Psr\Log\InvalidArgumentException('Invalid LogLevel: '.$level);

        if (is_object($message))
            $message = (string)$message;

        if (!empty($context))
            $message = $this->replaceContextInMessage($message, $context);
    }


    protected function isValidErrorLevel($level)
    {
        return in_array($level, $this->errorLevels);
    }


    protected function replaceContextInMessage($message, array $context)
    {
        foreach ($context as $key => $value)
        {
            $search = '{'.$key.'}';
            $message = str_replace($search, $value, $message);
        }

        return $message;
    }
}