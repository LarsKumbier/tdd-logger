<?php


namespace KITS\Log\LoggerTest;


class LoggerMock extends \KITS\Log\Logger
{
    private $logWasCalled = array();

    public function logWasCalled()
    {
        return $this->logWasCalled;
    }

    public function log($level, $message, array $context = array())
    {
        $options = array (
            'level' => $level,
            'message' => $message,
            'context' => $context
        );
        array_push($this->logWasCalled, $options);
        return parent::log($level, $message, $context);
    }

    public function callReplaceContextInMessage($message, $context = array())
    {
        return $this->replaceContextInMessage($message, $context);
    }
} 